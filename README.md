# Read Me

This is a collection of some of **my** stuff.

Please do **not** use any of the files in this repository without my consent!  
(scripts in `tools/` are fine, but use at your own risk)

## Tools

### Resize profile image script

```bash
# zsh
php tools/downscale_images.php images/profile/*.{png,jpg}(N)

# bash
php tools/downscale_images.php images/profile/*.{png,jpg}
```
