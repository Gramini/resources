<?php

$images = $argv;
array_shift($images);

const RESOLUTIONS = [
    64,
    128,
    150,
    192,
    256,
    384,
    512,
];

function generateOutputName(string $image, int $targetX, int $targetY): string
{
    $directory = dirname($image) . '/resized/';
    $name      = pathinfo($image, PATHINFO_FILENAME);
    $extension = pathinfo($image, PATHINFO_EXTENSION);

    $sizeInfo = $targetX === $targetY
        ? (string)$targetX
        : sprintf(
            '%dx%d',
            $targetX,
            $targetY,
        );

    return sprintf(
        '%s%s__%s.%s',
        $directory,
        $name,
        $sizeInfo,
        $extension,
    );
}

function downscaleImage(string $image, int $targetX, int $targetY)
{
    $rawOutput = generateOutputName($image, $targetX, $targetY);

    if (!is_dir(dirname($rawOutput))) {
        if (!mkdir($concurrentDirectory = dirname($rawOutput)) && !is_dir($concurrentDirectory)) {
            throw new RuntimeException(sprintf('Directory "%s" was not created', $concurrentDirectory));
        }
    }

    if (is_file($rawOutput)) {
        echo '— already present';
        return;
    }

    $input  = escapeshellarg($image);
    $output = escapeshellarg($rawOutput);

    $command = sprintf(
        'convert %s -resize %dx%d %s',
        $input,
        $targetX,
        $targetY,
        $output,
    );

    shell_exec($command);
    echo '— done';
}

function getDelimiter(int $index, int $maxIndex): string
{
    if ($maxIndex === 0) {
        return '╶';
    }

    switch ($index) {
        case 0:
            return '┌';
        default:
            return '├';
        case $maxIndex:
            return '└';
    }

//    return match ($index) {
//        0         => '┌',
//        default   => '├',
//        $maxIndex => '└',
//    };
}

function findLongestResolutionString(): int {
    return strlen((string)max(RESOLUTIONS)) * 2 +1;
}

function downscaleImages(array $images)
{
    foreach ($images as $image) {
        if (!is_file($image)) {
            echo sprintf(
                'File "%s" not found, skipping',
                $image,
            ), PHP_EOL;

            continue;
        }

        echo sprintf(
            'Processing "%s"',
            $image,
        ), PHP_EOL;

        $padLength = findLongestResolutionString() + 1;
        $maxIndex  = count(RESOLUTIONS) - 1;
        foreach (RESOLUTIONS as $index => $resolution) {
            $resolutionInfo = sprintf('%dx%d', $resolution, $resolution);

            echo sprintf(
                ' %s %s',
                getDelimiter($index, $maxIndex),
                str_pad($resolutionInfo, $padLength, ' ', STR_PAD_RIGHT),
            );

            downscaleImage($image, $resolution, $resolution);
            echo PHP_EOL;
        }

        echo PHP_EOL;
    }
}

downscaleImages($images);
